Problem Statement
With an abundance of food reviews, it is difficult to discern the main point of the review. What if the process of reading reviews and detecting positive or negative ones could be as simple as entering the page? To streamline the process of how we digest food reviews, I have created a Yelp review sentiment analyzer using the power of deep learning that highlights restaurant reviews based on their sentiment (green for positive; red for negative). This means you'll spend less time reading and more time eating.

Technologies Used
- Keras: Used to create and train a deep learning model consisting of LSTM, 1-D Convolutional, and Dense layers
- Amazon EC2: Used to streamline the process of training by training using their servers
- Flask: Used to create a REST API that allows anyone to retrieve data from the ML data
- Heroku: Used as a Platform as a Service provider that allowed us to deploy the Web API
- Docker: Used to package all of the requirements for Flask, Keras, Tensorflow to ensure consistency in deployment
- Javascript: Used to create script that handled text scraping from webpages
- CSS: Used to help change styles when certain events were launched

How I did it
- First, I researched into recurrent neural network architecture, specifically LSTMs, because an online class piqued my interest
- My research led me to a tutorial that taught people how to create a yelp sentiment analysis program and train models on Amazon EC2
- I followed the tutorial and ended up building a full scale RNN model that allowed accurate detection of sentiment 90% of the time
- I ended up tuning parameters such as the amount of words in the set, the dropout rates, and the amount of layers and garnered a slightly higher accuracy value
- After building the model on the cloud, I was interested in deploying the model because I wanted the ML service to be available across any platform 
- From past experience using Python Flask, I had experience routing URLs, working with JSON, and posting and retrieving data from a Flask server
- I created a Flask REST API that takes in a string of the text query and returns sentiment in the form of a JSON object
- Afterwards, I used Heroku to deploy on their servers and make the ML app available to all types of interfaces
- In order to embed ML libraries like Keras, TF, and sci-kit learn into the web application, I used a docker image that contained a distribution of Miniconda. I then ran commands in a Docker file that downloaded the rest of the dependencies
- Last, I created a content-script in Javascript that matched web pages that started with: https://www.yelp.com/biz/
- The web application makes a GET request to the ML Heroku/Flask server and does so for every single review
- Each review is then highlighted based on sentiment, which allows users a simple visual experience with sentiment

Author
- Ryan Luu

Acknowledgements
- Thank you to the creators of CORS anywhere Heroku App since it allowed me to make REST API calls from the client side
- Special thanks to amazing open source software like Keras and Tensorflow

Additional Comments
- The server is up and running at: https://sentiyelpanalysisml.herokuapp.com/sentiyelp/api/v1/predict/
- You can test out the machine learning capabilities by adding any text after the last front slash in the URL above
- Let me know what you think about the REST API and this overall project
