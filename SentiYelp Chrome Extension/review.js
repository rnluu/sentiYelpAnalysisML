//Initializes the sentiment at neutral
var sentiment=0;

//Gets review objects from page
function getReviews(){
return document.getElementsByClassName("review-content");
}

//Checks every review text for sentiment
function checkReviews(reviews){
    //Loops through each review and checks
    for (var i=0;i<reviews.length;i++){
        
        reviewText=reviews[i].innerText
        processText(reviewText,reviews,i);
        
        //Checks to see if overall sentiment is ready to be gauged
        if(i==reviews.length-1){
            displayOverallSentiment();
        }
    }
}

//Determines the sentiment towards a restaurant
function displayOverallSentiment(){
        
    if(sentiment>0){
        alert("People generally have a positive sentiment towards this place");
        return;
    }
    
    else if (sentiment<0){
        alert("People generally have a negative sentiment towards this place")
        return;
    }
    
    alert("People generally have a neutral sentiment towards this place");
}

//Processes the data obtained from the web page
function processText(sentiText,reviews,i){
    sentiText=prepareText(sentiText);
    console.log(sentiText);
    
    //Retrieves information from the initializer
    var response=initMLAPI(sentiText);
    var url=response[0]; //Url for the CORS request
    var header=response[1]; //Header for the CORS

    //Fetches data from the API
    fetchData(url,header,reviews,i);
}

//Function that joins each word of the text with "+"
function prepareText(text){
    //Removes all special characters
    text = text.replace(/[^a-zA-Z ]/g, '')
    //Joins spaces with pluses
    return text.split(" ").join("+");
}

//Fetches the data from the REST API
function fetchData(url,init,reviews,i){
    fetch(url,init).then(function(response){
        
        //Checks for unsuccessful response
        if (response.status !=200){
            console.log("Something went wrong!")
            return;
        }
        
        //Checks for successful response
        response.json().then(function(data){
            predictAction(data["sentiment"],reviews,i);
            
        });
        
    //Catches the errors and logs them
    }).catch(function(err){
        
        console.log(err)
    });
}

//Performs various actions on pages following the prediction
function predictAction(predSentiment,reviews,i){
    if (predSentiment=="Positive"){
        sentiment+=1;
        reviews[i].style.background="#E5FFE5";
    }
    
    else if(predSentiment=="Negative"){
        sentiment-=1;
        reviews[i].style.background="#FFC0CB"
    }
    console.log(sentiment);
    
}
    
//Sets up the API call by creating a header object, CORS functionality and a call to a CORS server
function initMLAPI(sentiText){
    
    //Sets the CORS server url and ML api url
    var serverUrl="https://cors-anywhere.herokuapp.com/"
    var apiUrl="https://sentiyelpanalysisml.herokuapp.com/sentiyelp/api/v1/predict/"+sentiText    
    
    
    finalApiUrl=serverUrl+apiUrl;
    
    //Creates header that allows for COR request
    var header=new Headers();
    header.append("Allow-Control-Allow-Origin","*");
    var init={
        method:"GET",
        headers:header,
        mode:"cors",
        cache:"default"
    }
    
    return [finalApiUrl,init]
}

//Main functions calls
function main(){
reviews=getReviews();
checkReviews(reviews);}

main();

