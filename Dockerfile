FROM heroku/miniconda:4.3.27



#Grab requirements.txt

ADD ./SentApp/requirements.txt /tmp/requirements.txt

RUN pip install -qr /tmp/requirements.txt

#Adding our code. Change first SentApp to webApp if it doesn't work
ADD ./SentApp /opt/SentApp
WORKDIR /opt/SentApp

RUN conda install tensorflow
RUN conda install keras

CMD gunicorn --bind 0.0.0.0:$PORT wsgi

