# -*- coding: utf-8 -*-
"""
Created on Sat Dec 16 02:47:52 2017

@author: Ryanluu2017
"""

from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences

tokenizer=Tokenizer(num_words=10)

texts=["This is a common word","So is this is","This should show up many many many this times","this this this"]

tokenizer.fit_on_texts(texts)
sequences=tokenizer.texts_to_sequences(texts)
print(sequences)

padded_sequences=pad_sequences(sequences)
print(padded_sequences)

'''
#Takes in a list of strings, concatenates and then prints out the instances of each
def tokenize(text_list):
    frequencies={}
    text_list=" ".join(text_list).split(" ")
    for text in text_list:
        if text in frequencies:
            frequencies[text]+=1
        else:
            frequencies[text]=1
    return frequencies
    
print(tokenize(texts)) '''

vecs=[[1, 2, 4, 5, 6], [7, 2, 1, 2], [1, 8, 9, 3, 3, 3, 1], [1, 1, 1]]

#Inverse padding sequence to make matrices of equal size so that our data inputs are consistent
def pad_sequence(vectors):
    #Determine max length vector first
    maximum=determine_max_length(vectors)
    
    
    #Pad the vector to the maximum value
    for i in range(len(vectors)):
        if len(vectors[i])<maximum:
            for j in range(maximum-len(vectors[i])):
                vectors[i].append(0)
    
    return vectors
    
def determine_max_length(vectors):
    maximum=0
    for vector in vectors:
        if len(vector)> maximum:
            maximum=len(vector)
    return maximum
    
pad_sequence(vecs)