from flask import Flask, render_template,request,jsonify
from keras.models import load_model
from keras.preprocessing.sequence import pad_sequences
import pickle

app=Flask(__name__)

#Intializes the beginning sentiment
sentiment=None
tokenizer=None
predictor=None
    
def load_ml_model():
    global tokenizer,predictor
    #Opens the pickle of the tokenizer
    with open("tokenizer.pickle","rb") as f:
        tokenizer=pickle.load(f)
        
    #Loads model
    predictor=load_model("yelp_sentiment_analyzer.hdf5") 

#Loads the ml model using a function
load_ml_model()

#Routes to the root url and allows get and post methods
@app.route("/sentiyelp/api/v1/predict/<path:req_text>",methods=["GET","POST"])
def main(req_text): #req_test is the requested text from the API call
    
    
    try:
        req_text=" ".join(req_text.split("+"))
        prediction=predict_model(req_text)
        return jsonify(prediction)
    #return render_template("index.html",sentiment=sentiment)
    except:
        return jsonify({"error":"not found"})
    
    
#Predicts sentiment using the model
def predict_model(req_text):
    global tokenizer,predictor
    
    #Creates a new list for the requested text
    req_text_list=[]
    
    #Prepares the words for processing
    req_text_list.append(req_text)
    tokenized_req_text_list=tokenizer.texts_to_sequences(req_text_list)
    data=pad_sequences(tokenized_req_text_list,maxlen=300)
    
    #Get predictions
    prediction=predictor.predict(data)
    return(categorical_prediction(prediction))
    
#Categorizes predictions based on cutoff thresholds
def categorical_prediction(prediction):
    if prediction[0][0]<=0.30:
        return {"sentiment":"Negative"}
    elif prediction[0][0]>=0.70:
        return {"sentiment":"Positive"}
    return {"sentiment":"Neutral"}
        
if __name__=="__main__":
    app.run()